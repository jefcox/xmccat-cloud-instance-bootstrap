#!/bin/bash

source includes.sh
source config.sh
source util.sh

NODE_TYPE=$1
CWD=$(pwd)
PREFIX=""

## Setup rabbitmq server
python rabbitmqadmin.py -H $RABBIT_MQ_SERVER declare exchange name=$INSTANCE_STATUS_EXCHANGE type=direct
python rabbitmqadmin.py -H $RABBIT_MQ_SERVER declare queue name=$INSTANCE_STATUS_FAIL durable=true
python rabbitmqadmin.py -H $RABBIT_MQ_SERVER declare binding source=$INSTANCE_STATUS_EXCHANGE destination_type=queue destination=$INSTANCE_STATUS_FAIL routing_key="fail"
python rabbitmqadmin.py -H $RABBIT_MQ_SERVER declare queue name=$INSTANCE_STATUS_WARN durable=true
python rabbitmqadmin.py -H $RABBIT_MQ_SERVER declare binding source=$INSTANCE_STATUS_EXCHANGE destination_type=queue destination=$INSTANCE_STATUS_WARN routing_key="warn"
python rabbitmqadmin.py -H $RABBIT_MQ_SERVER declare queue name=$INSTANCE_STATUS_INFO durable=true
python rabbitmqadmin.py -H $RABBIT_MQ_SERVER declare binding source=$INSTANCE_STATUS_EXCHANGE destination_type=queue destination=$INSTANCE_STATUS_INFO routing_key="info"

publishStatus "started" "Instance started" "info"
publishStatus "verifying" "Verify instance configuration" "info"

## Call scripts ##
echo
$PREFIX $CWD/check_platform.sh
echo
$PREFIX $CWD/install_pkg_deps.sh all_deps.txt
echo
if [ $NODE_TYPE == "mysql_node" ]; then
	$PREFIX $CWD/install_pkg_deps.sh mysql_node_deps.txt
elif [ $NODE_TYPE == "data_node" ]; then
	$PREFIX $CWD/install_pkg_deps.sh data_node_deps.txt
elif [ $NODE_TYPE == "mgmt_node" ]; then
	$PREFIX $CWD/install_pkg_deps.sh mgmt_node_deps.txt
fi

