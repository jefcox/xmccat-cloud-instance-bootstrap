#!/bin/bash

case $(uname -m) in
x86_64)
    ARCH=x64  # or AMD64 or Intel64 or whatever
    ;;
i*86)
    ARCH=x86  # or IA32 or Intel32 or whatever
    ;;
*)
    # leave ARCH as-is
    ;;
esac

if [ -f /etc/lsb-release ]; then
    . /etc/lsb-release
    OS=$DISTRIB_ID
    VER=$DISTRIB_RELEASE
elif [ -f /etc/redhat-release ]; then
#Red Hat Enterprise Linux Server release 5.8 (Tikanga)
	OS=`cat /etc/redhat-release | awk {'print $1 $2}'`
	VER=`cat /etc/redhat-release | awk {'print $7}'`
else
    OS=$(uname -s)
    VER=$(uname -r)
fi

# Text color variables
txtund=$(tput sgr 0 1)          # Underline
txtbld=$(tput bold)             # Bold
txtred=$(tput setaf 1) 			#  red
txtgre=$(tput setaf 2) 			#  green
txtyel=$(tput setaf 3) 			#  yellow
txtblu=$(tput setaf 4) 			#  blue
txtpur=$(tput setaf 5) 			#  purple
txtcya=$(tput setaf 6) 			#  purple
txtwht=$(tput setaf 7) 			#  white
bldred=${txtbld}$(tput setaf 1) #  red
bldgre=${txtbld}$(tput setaf 2) #  green
bldyel=${txtbld}$(tput setaf 3) #  yellow
bldblu=${txtbld}$(tput setaf 4) #  blue
bldpur=${txtbld}$(tput setaf 5) #  purple
bldcya=${txtbld}$(tput setaf 6) #  purple
bldwht=${txtbld}$(tput setaf 7) #  white
txtrst=$(tput sgr0)             # Reset
txthdr=${bldpur}				# header color

# Feedback indicator variables
fail=[${bldred}xx${txtrst}]
info=[${bldgre}**${txtrst}]
warn=[${bldyel}!!${txtrst}]
pass=[${bldcya}--${txtrst}]
ques=[${bldpur}??${txtrst}]

