
HOSTNAME=$(hostname)
INTERFACE=eth0
IPADDRESS=$(ifconfig $INTERFACE | grep inet | grep -v inet6 | cut -d ":" -f 2 | cut -d " " -f 1)
RABBIT_MQ_SERVER=12.192.18.230
INSTANCE_STATUS_EXCHANGE=xmccat_cloud_server.instanceStatus
INSTANCE_STATUS_FAIL=xmccat_cloud_server.instanceStatus.fail
INSTANCE_STATUS_WARN=xmccat_cloud_server.instanceStatus.warn
INSTANCE_STATUS_INFO=xmccat_cloud_server.instanceStatus.info
