#!/bin/bash

source includes.sh
source util.sh

PREFIX=""


echo
publishStatus "verifying" "Checking platform dependencies" "info"

if [ $OS == 'RedHat' ]; then

	if [ ${VER:0:1} == 5 ]; then
		publishStatus "verifying" "$OS $VER: OK" "info"
	elif [ ${VER:0:1} == 6 ]; then
		publishStatus "verifying" "$OS $VER: OK" "info"
	else
		publishStatus "verifying" "$OS $VER: not currently supported" "fail"
		exit 1
	fi
	
elif [ $OS == 'Ubuntu' ]; then
	
	if [ ${VER:0:2} == 12 ]; then
		publishStatus "verifying" "$OS $VER: OK" "info"
	else
		publishStatus "verifying" "$OS $VER: not currently supported" "fail"
		exit 1
	fi
	
else
	publishStatus "verifying" "$OS $VER: not recognized" "fail"
	exit 1
fi

if [ $ARCH != 'x64' ]; then
	publishStatus "verifying" "$ARCH: not currently supported" "fail"
	exit 1
else
	publishStatus "verifying" "$ARCH: OK" "info"
fi
