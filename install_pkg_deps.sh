#!/bin/bash

source includes.sh
source util.sh

## Script to install/remove xmccat cloud server dependencies ##
## Must be run as root ## 
## Dependencies must be listed in file deps.txt ## 
DEPS_FILE=$1

function installDepRedHat()
{
	if rpm -q $1 &> /dev/null
	then 
		description="`rpm -q $1 | tr '\n' ' '`: OK "
		publishStatus "verifying" "$description" "info"
	else
		publishStatus "verifying" "$1: NO" "info"
		publishStatus "verifying" "$1: Installing" "info"
		if [ `sudo yum install -y -v $1 &> /tmp/pkgDeps.log` ]; then
			pkgDepsLog=$(cat /tmp/pkgDeps.log)
			publishStatus "verifying" "$pkgDepsLog" "fail"
		fi	
		installDepRedHat $1
	fi
}

function installDepUbuntu()
{
	if dpkg -s $1 &> /dev/null
	then 
		description="`dpkg-query -W $1 | tr '\n' ' '`: OK "
		publishStatus "verifying" "$description" "info"
	else
		publishStatus "verifying" "$1: NO" "info"
		publishStatus "verifying" "$1: Installing" "info"
		if [ `apt-get -qqy install $1 &> /tmp/pkgDeps.log` ]; then
			pkgDepsLog=$(cat /tmp/pkgDeps.log)
			publishStatus "verifying" "$pkgDepsLog" "fail"
		fi	
		installDepUbuntu $1
	fi
}

## change to using exchange for info, warn and fail.
publishStatus "verifying" "Checking package dependencies" "info"

for dep in $(cat $DEPS_FILE)
do
	if [ $OS == 'RedHat' ]; then
		installDepRedHat $dep

    elif [ $OS == 'Ubuntu' ]; then
        installDepUbuntu $dep
    fi
	
done
